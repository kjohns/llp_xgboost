import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.colors as colors

rng = np.random.default_rng()

nevt = 5000
xvar_signal = np.array([])
yvar_signal = np.array([])
xvar_bkg = np.array([])
yvar_bkg = np.array([])

for i in range(nevt):
    xvar_signal = np.append(xvar_signal,rng.normal(12.,2.))
    yvar_signal = np.append(yvar_signal,rng.normal(12.,2.))
    xvar_bkg = np.append(xvar_bkg,rng.normal(9.,3.))
    yvar_bkg = np.append(yvar_bkg,rng.normal(9.,3.))
    xvar_all = np.concatenate((xvar_signal,xvar_bkg), axis=0)
    yvar_all = np.concatenate((yvar_signal,yvar_bkg), axis=0)


asum = 0
bsum = 0
csum = 0
dsum = 0
esum = 0
fsum = 0
xcut = 11.
ycut = 11.

for i in range (xvar_bkg.shape[0]):
    if (xvar_bkg[i] >= xcut and yvar_bkg[i] >= ycut): asum = asum + 1
    if (xvar_bkg[i] < xcut and yvar_bkg[i] >= ycut): bsum = bsum + 1
    if (xvar_bkg[i] >= xcut and yvar_bkg[i] < ycut): csum = csum + 1
    if (xvar_bkg[i] < xcut and yvar_bkg[i] < ycut): dsum = dsum + 1
# esum should be the same as asum
    if (xvar_bkg[i] >= xcut and yvar_bkg[i] >= ycut): esum = esum + 1
print (asum, bsum, csum, dsum, esum, (asum + bsum + csum + dsum))
esum_abcd = float(csum) * float(bsum) / float(dsum)
print (esum, esum_abcd)

# error
d_asum = np.sqrt(asum)
d_bsum = np.sqrt(bsum)
d_csum = np.sqrt(csum)
d_dsum = np.sqrt(dsum)
d_esum = np.sqrt(esum)
d_esum_abcd = esum_abcd * np.sqrt( (d_csum/csum)**2 + (d_bsum/bsum)**2 + (d_dsum/dsum)**2 )

print (esum, d_esum, esum_abcd, d_esum_abcd)

ichoose = 0
if (ichoose == 0):
    fig, ax = plt.subplots(2,2)

    h = ax[1,0].hist2d(xvar_all, yvar_all, bins=50, norm=colors.LogNorm()\
, cmap='jet')
    ax[1,0].set_xlim(0,20)
    ax[1,0].set_ylim(0,20)
    ax[1,0].set(title='abcd all', xlabel='xvar', ylabel='yvar')
    plt.colorbar(h[3],ax=ax[1,0])

    h = ax[0,0].hist2d(xvar_signal, yvar_signal, bins=50, norm=colors.LogNorm()\
, cmap='jet')
    ax[0,0].set_xlim(0,20)
    ax[0,0].set_ylim(0,20)
    ax[0,0].set(title='abcd signal', xlabel='xvar', ylabel='yvar')
    plt.colorbar(h[3],ax=ax[0,0])

    h = ax[0,1].hist2d(xvar_bkg, yvar_bkg, bins=50, norm=colors.LogNorm()\
, cmap='jet')
    ax[0,1].set_xlim(0,20)
    ax[0,1].set_ylim(0,20)
    ax[0,1].set(title='abcd background', xlabel='xvar', ylabel='yvar')
    plt.colorbar(h[3],ax=ax[0,1])

    plt.tight_layout()
    plt.show()

